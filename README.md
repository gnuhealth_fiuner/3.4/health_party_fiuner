# health_party_fiuner-3.2.0

GNU Health party (fiuner) module
################################

This module add support to:

     * Modify education selection field, adding new options according to 
     argentine education degrees on party, SES, and patient registers
     
     *Add a model named Partido and a field name partido referenced to that model
